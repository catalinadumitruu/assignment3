const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const mysql = require('mysql2/promise')

const DB_USERNAME = 'root'
const DB_PASSWORD = ''//'welcome12#'

let conn

mysql.createConnection({
    user : DB_USERNAME,
    password : DB_PASSWORD
})
.then((connection) => {
    console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    conn = connection
    return connection.query('CREATE DATABASE IF NOT EXISTS tw_exam')
})
.then(() => {
    return conn.end()
})
.catch((err) => {
    console.warn(err.stack)
})

const sequelize = new Sequelize('tw_exam', DB_USERNAME, DB_PASSWORD,{
    dialect : 'mysql',
    logging: false
})

let Student = sequelize.define('student', {
    name : Sequelize.STRING,
    address : Sequelize.STRING,
    age : Sequelize.INTEGER
},{
    timestamps : false
})


const app = express()
app.use(bodyParser.json())

app.get('/create', async (req, res) => {
    try{
        await sequelize.sync({force : true})
        for (let i = 0; i < 10; i++){
            let student = new Student({
                name : 'name ' + i,
                address : 'some address on ' + i + 'th street',
                age : 30 + i
            })
            await student.save()
        }
        res.status(201).json({message : 'created'})
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})
    }
})

app.get('/students', async (req, res) => {
    try{
        let students = await Student.findAll()
        res.status(200).json(students)
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})        
    }
})

app.post('/students', async (req, res) => {
    try{
        const props = Object.getOwnPropertyNames(req.body);

        // console.log("****        THIS IS THE REQUEST   ****   " + req.body.name + " " + req.body.address + " " + req.body.age)
        // console.log("$$$$$$$$$$    SIZE OF PROPS   " + props.length);
        // for(let i = 0 ; i < props.length ; i++){
        //     console.log(" %%%   prop   %%%  " + props[i]);
        // }

        if(props.length == 0 || !req.body){
            res.status(400).json({
                message : "body is missing"
            })
        } else if(!props.includes('name') ||  !props.includes('address') ||  !props.includes('age')){
            res.status(400).json({
                 message : "malformed request"
            })
        }

      // console.log("ksldjkfdnlanglkna0    " + Object.getOwnPropertyNames(req.body).length);

            if(req.body.age < 0){
                res.status(400).json({
                    "message": "age should be a positive number"
                });
            }

        // let stud = new Student({
        //     name: req.body.name,
        //     address : req.body.address,
        //     age : req.body.age
        // });

        // if(!stud){
        //     stud.build();
        //    await stud.save();
        //     res.status(201).json({
        //         message : "created"
        //     })
        // }

        //    if(req.body.name && req.body.address && req.body.age && req.body.age > 0 && req.body.name.length > 0 && req.body.address.length > 0){
        //         //  Student.build({
        //         //     name : req.body.name,
        //         //     address : req.body.address,
        //         //     age : req.body.age
        //         // }).save();
        //         let stud = new Student({
        //             name: req.body.name,
        //             address : req.body.address,
        //             age : req.body.age
        //         });
        //         stud.save();
        //         res.status(201).json({
        //             message : "created"
        //         });
            //} 
                if(req.body.name && req.body.address && req.body.age && req.body.age > 0 && req.body.name.length > 0 && req.body.address.length > 0){
                   Student.create(req.body);
                    res.status(201).json({ message : "created"});
                }

        Student.count().then(r => {
           console.log("£££££££££££   NUMBER OF STUDS     ££££££  " + r)
         });
    }
    catch(err){
        console.warn(err.stack) 
        res.status(500).json({message : 'server error'})        
    }

}) 

module.exports = app